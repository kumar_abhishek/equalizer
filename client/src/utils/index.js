const perc2color = (perc) => {
	var r, g, b = 0;
	if(perc < 50) {
		r = 255;
		g = Math.round(5.1 * perc);
	}
	else {
		g = 255;
		r = Math.round(510 - 5.10 * perc);
	}
	return `rgb(${r}, ${g}, ${b})`;
};

export {perc2color};