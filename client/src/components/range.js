const $ = React.createElement;

export default () => (
	$('div', {class:'range flex justsb flexc'}, 
		$('span', null, '-12db'),
		$('span', null, '0'),
		$('span', null, '+12db')
	)
);
